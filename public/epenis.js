function draw_chart(names, series, guild, height) {
  console.log(series);
  const config = {
    type: 'bar',
    data: {
      labels: names,
      datasets: [
        {
          label: 'BANE',
          data: series[0].data,
          backgroundColor: '#0080ff',
        },
        {
          label: 'LOD',
          data: series[1].data,
          backgroundColor: '#b048f8',
        },
      ],
    },
    options: {
      indexAxis: 'y',
      plugins: {
        title: {
          display: true,
          text: `${guild} E-penis Length`
        },
      },
      responsive: true,
      scales: {
        x: {
          stacked: true,
        },
        y: {
          stacked: true
        }
      }
    }
  };
  const ctx = document.getElementById('myChart').getContext('2d');
  const myChart = new Chart(ctx, config);
}

function epenis(file, lod_count_filter, height) {
  fetch(file)
    .then(response => response.json())
    .then(data => {
      var players_new = [];
      var characters_new = [];
      var alt_table = '<h3>Players and Alts</h3><table class="table table-striped">';
      var chars_table = '<h3>Top Characters</h3><ol>';

      Object.keys(data.players).sort().forEach(player => {
        var bane_kills = 0;
        var lod_kills = 0;

        var alt_names = data.players[player].filter(o => o.name != player).map(o => o.name).join(', ');
        alt_table += '<tr><td>' + player + '</td><td>' + alt_names + '<td></tr>';
        for (alt in data.players[player]) {
          lod_kills += data.players[player][alt]['experience']['LOD'];
          bane_kills += data.players[player][alt]['experience']['BANE'];
        }

        if (lod_kills + bane_kills >= lod_count_filter) {
          players_new.push({
            'NAME': player,
            'LOD': lod_kills,
            'BANE': bane_kills,
          });
        }
      });

      Object.keys(data.players).forEach(player => {
        for (alt in data.players[player]) {
          characters_new.push(data.players[player][alt]);
        }
      });

      players_new.sort((a, b) => (b['LOD'] + b['BANE']) - (a['LOD'] + a['BANE']));
      characters_new.sort((a, b) => b['experience']['LOD'] - a['experience']['LOD']);
      characters_new.slice(0, 100).map(c => {
        chars_table += '<li>' + c['name'] + (c['main'] ? ' (' + c['main'] + ')' : '') + ': ' + c['experience']['LOD'] + ' LOD</li>';
      });
      chars_table += '</ol>';

      var series = [{
        'name': 'BANE',
        'data': players_new.map((o) => o['BANE']),
      }, {
        'name': 'LOD',
        'data': players_new.map((o) => o['LOD']),
      }];
      draw_chart(players_new.map((o) => o['NAME']), series, data.guild, height);
      alt_table += '</table>';
      document.querySelector("#alts").innerHTML = alt_table;
      document.querySelector("#characters").innerHTML = chars_table;
      document.title = `${data.guild} E-Penis Length`;
    });
}
