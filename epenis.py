import xml.etree.ElementTree as ET
import sys
from pprint import pprint
import json
import requests
from datetime import datetime
from bs4 import BeautifulSoup
import logging


class EPenis:
    def __init__(self):
        self.players = {}
        self.guild = ''

    def parse_file(self, filename):
        self.tree = ET.parse(filename)
        self.root = self.tree.getroot()
        self.guild = self.root.get('guild')

        for player in self.root.iter('PLAYER'):
            main = player.get('main')
            if not main:
                main = player.get('name')
            if main not in self.players:
                self.players[main] = [ player.attrib ]
            else:
                self.players[main].append(player.attrib)

    def collect_experience(self):
        for player in self.players.keys():
            for player in self.players[player]:
                player['experience'] = self._get_experience(player['name'])

    def export(self):
        with open('{}.json'.format(self.guild.replace(' ', '_')), 'w') as fp:
            fp.write(json.dumps({
                'timestamp': datetime.now().isoformat() + 'Z',
                'guild': self.guild,
                'players': self.players
            }, indent=2))

    def _get_experience(self, character, realm='Icecrown'):
        logging.debug('Checking {}-{}'.format(character, realm))
        experience = {
            'LOD': 0,
            'BANE': 0,
        }
        url = "http://armory.warmane.com/character/{}/{}/statistics".format(character, realm)
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
        }
        res = requests.post(url, data={'category': 15062}, headers=headers)

        try:
            soup = BeautifulSoup(json.loads(res.text)['content'], 'html.parser')
        except Exception:
            return experience

        for line in soup.find_all('tr'):
            cols = line.find_all('td')
            if len(cols) < 2:
                continue
            try:
                if cols[0].text == 'Victories over the Lich King (Heroic Icecrown 10 player)':
                    experience['BANE'] = int(cols[1].text)
                elif cols[0].text == 'Victories over the Lich King (Heroic Icecrown 25 player)':
                    experience['LOD'] = int(cols[1].text)
            except ValueError:
                pass

        return experience




if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    epenis = EPenis()
    epenis.parse_file(sys.argv[1])
    epenis.collect_experience()
    epenis.export()
